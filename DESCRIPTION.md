## About

Tiny Tiny RSS is an open source web-based news feed (RSS/Atom) reader and aggregator, designed to allow you to read news from any location, while feeling as close to a real desktop application as possible.

# Features

* Server-side AJAX-powered application, user only needs a web browser.
* Supports
  * feed aggregation / syndication,
  * keyboard shortcuts,
  * OPML import/export,
  * multiple ways to share stuff: via RSS feeds, using plugins to various social sites, sharing by URL, etc,
  * sharing arbitrary content through tt-rss,
  * mobile devices,
  * internationalization,
  * various plugins and themes,
  * detecting and filtering duplicate articles,
  * podcasts,
  * flexible article filtering,
  * JSON-based API,
  * and much more...
* [Android client](https://play.google.com/store/apps/details?id=org.ttrssreader)
* Free software, licensed under GNU GPLv3

