#!/bin/bash

echo "==> Updating feeds"

source /run/ttrss/env.sh
/usr/local/bin/gosu www-data:www-data php /app/code/update.php --feeds

