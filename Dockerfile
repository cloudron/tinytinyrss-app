FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

# https://gitlab.tt-rss.org/tt-rss/tt-rss and mirrored at https://git.tt-rss.org/fox/tt-rss.git/log/
# renovate: datasource=git-refs packageName=https://gitlab.tt-rss.org/tt-rss/tt-rss branch=master
ARG TTRSS_COMMIT=532570ca17f1120a4bfc07195080e5e6a3c469fd

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN curl -H 'User-Agent: Mozilla/5.0' -L https://gitlab.tt-rss.org/tt-rss/tt-rss/-/archive/${TTRSS_COMMIT}/tt-rss.tar.gz | tar -xz --strip-components 1 -f - -C /app/code && \
    chown -R www-data:www-data /app/code && \
    mv /app/code/cache /app/code/cache.template && ln -s /app/data/cache /app/code/cache && \
    mv /app/code/lock /app/code/lock.template && ln -s /run/ttrss/lock /app/code/lock && \
    ln -s /app/data/config.php /app/code/config.php && \
    rm -rf /app/code/plugins.local && ln -s /app/data/plugins.local /app/code/plugins.local && \
    rm -rf /app/code/themes.local && ln -s /app/data/themes.local /app/code/themes.local && \
    rm -rf /app/code/feed-icons && ln -s /app/data/feed-icons /app/code/feed-icons

# https://gitlab.tt-rss.org/tt-rss/plugins/ttrss-mailer-smtp
# renovate: datasource=git-refs packageName=https://gitlab.tt-rss.org/tt-rss/ttrss-mailer-smtp branch=master
ARG MAILER_COMMIT=c5d21870c7ca9f841abaf51c7dce671a542f4a5e
RUN mkdir -p /app/code/plugins/mailer_smtp && \
    curl -H 'User-Agent: Mozilla/5.0' -L https://gitlab.tt-rss.org/tt-rss/ttrss-mailer-smtp/-/archive/${MAILER_COMMIT}/ttrss-mailer.tar.gz | tar -xz --strip-components 1 -f - -C /app/code/plugins/mailer_smtp && \
    chown -R www-data:www-data /app/code/plugins/mailer_smtp

# https://gitlab.tt-rss.org/tt-rss/plugins/ttrss-auth-oidc
# renovate: datasource=git-refs packageName=https://gitlab.tt-rss.org/tt-rss/ttrss-auth-oidc branch=master
ARG OIDC_COMMIT=9ef6658d3d613268b1de3e383c4b487aee4b78ac
RUN mkdir -p /app/code/plugins/auth_oidc && \
    curl -H 'User-Agent: Mozilla/5.0' -L https://gitlab.tt-rss.org/tt-rss/ttrss-auth-oidc/-/archive/${OIDC_COMMIT}/ttrss-auth-oidc.tar.gz | tar -xz --strip-components 1 -f - -C /app/code/plugins/auth_oidc && \
    chown -R www-data:www-data /app/code/plugins/auth_oidc

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/tinytinyrss.conf /etc/apache2/sites-enabled/tinytinyrss.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/ttrss/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

COPY start.sh env.sh.template config.php.template update_feeds.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
