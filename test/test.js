#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(650000);

    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function subscribe() {
        const actions = '//span[@title="Actions..."]';

        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
        await browser.findElement(By.xpath(actions)).click();
        await browser.wait(until.elementLocated(By.xpath('//*[text() = "Subscribe to feed..."]')), TIMEOUT);
        await browser.findElement(By.xpath('//*[text() = "Subscribe to feed..."]')).click();
        await browser.wait(until.elementLocated(By.id('feedDlg_feedUrl')), TIMEOUT);
        await browser.sleep(3000);
        await browser.findElement(By.id('feedDlg_feedUrl')).sendKeys('https://reddit.com/.rss');
        await browser.findElement(By.xpath('//span[@role="button"]/span[contains(text(), "Subscribe")]')).click();
        await browser.sleep(5000);
    }

    async function loadsFeed() {
        console.log('Waiting for feeds. This can take up to 10 minutes', new Date());

        for (let times = 0; times < 15; ++times) {
            console.log('Loading the feed page');

            // first feed has id 2
            await browser.get('https://' + app.fqdn + '/#f=2&c=0');
            await browser.sleep(3000);
            const title = await browser.getTitle();
            const m = title.match(/(\(.*\)) Tiny Tiny RSS/);
            if (m) {
                console.log('Loaded ' + m[1] + ' articles');
                return;
            }

            console.log(`not done yet. title: ${title}`);
            await browser.sleep(60 * 1000);
        }

        throw new Error('Could not load feeds');
    }

    async function canFetchIcon() {
        // first feed has id 1
        let response;
        if (app.manifest.version == '2.6.0') {
            response = await superagent.get('https://' + app.fqdn + '/feed-icons/1.ico');
        } else {
            response = await superagent.get(`https://${app.fqdn}/public.php?op=feed_icon&id=1`);
        }
        expect(response.status).to.equal(200);
    }

    async function cannotAdminLogin() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
        await browser.findElement(By.id('login')).sendKeys('admin');
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys('password');
        await browser.findElement(By.tagName('form')).submit();
        await browser.wait(until.elementLocated(By.xpath('//div[contains(text(), "Incorrect username or password")]')), TIMEOUT);
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        await waitForElement(By.id('widget_login'));

        await browser.findElement(By.xpath('//span[@widgetid="dijit_form_Button_1"]/span[contains(., "Log in with")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(5000); // ttrss takes a while to load
        await browser.wait(until.elementLocated(By.xpath('//span[text()="All articles"]')), TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
        const actions = '//span[@title="Actions..."]';
        await browser.findElement(By.xpath(actions)).click();
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//*[text() = "Logout"]')).click();
        await browser.sleep(5000);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];

        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('cannot login as admin', cannotAdminLogin);
    it('can user login OIDC', loginOIDC.bind(null, username, password, false));
    it('has no articles', async function () {
        await browser.wait(until.elementLocated(By.xpath('//*[contains(text(), "No articles found to display.")]')), TIMEOUT);
    });
    it('can subscribe', subscribe);
    it('loads the feed', loadsFeed);
    it('can fetch the icon', canFetchIcon);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('cannot login as admin', cannotAdminLogin);
    it('can user login OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can user login OIDC', loginOIDC.bind(null, username, password));
    it('loads the feed', loadsFeed);
    it('can fetch the icon', canFetchIcon);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can user login OIDC', loginOIDC.bind(null, username, password));
    it('loads the feed', loadsFeed);
    it('can fetch the icon', canFetchIcon);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app old version', function () {
        execSync('cloudron install --appstore-id org.tt_rss.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can user login OIDC', loginOIDC.bind(null, username, password));
    it('can subscribe', subscribe);
    it('loads the feed', loadsFeed);
    it('can fetch the icon', canFetchIcon);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });

    it('can user login OIDC', loginOIDC.bind(null, username, password));
    it('loads the feed', loadsFeed);
    it('can fetch the icon', canFetchIcon);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
