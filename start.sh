#!/bin/bash

set -eu

mkdir -p /run/ttrss/sessions /run/ttrss/lock /run/ttrss/cache /app/data/{cache,feed-icons} /app/data/plugins.local /app/data/themes.local

# https://community.tt-rss.org/t/favicons-cache-implementation-overhaul . feed-icons have to be backed up, so might as well backup the whole cache
rsync -a /app/code/cache.template/. /app/data/cache
cp -r /app/code/lock.template/. /run/ttrss/lock

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/env.sh ]]; then
    cp /app/pkg/env.sh.template /app/data/env.sh
fi

# migration removing unnecessary exports. can be removed in next release
sed -e '/TTRSS_DB/d;/TTRSS_MYSQL/d;/TTRSS_SINGLE_USER_MODE/d;/TTRSS_SIMPLE_UPDATE_MODE/d;/TTRSS_SELF/d' \
    -e '/TTRSS_AUTH/d;/TTRSS_LOG/d;/TTRSS_CHECK_FOR_UPDATES/d;' \
    -e '/TTRSS_SMTP/d;/TTRSS_DIGEST/d;/^CLOUDRON_BUILTIN_PLUGINS/d;' \
    -e '/# auth/d;/# do not change this/d;' \
    -i /app/data/env.sh

export TTRSS_DB_TYPE=mysql
export TTRSS_DB_HOST=$CLOUDRON_MYSQL_HOST
export TTRSS_DB_USER=$CLOUDRON_MYSQL_USERNAME
export TTRSS_DB_NAME=$CLOUDRON_MYSQL_DATABASE
export TTRSS_DB_PASS=$CLOUDRON_MYSQL_PASSWORD
export TTRSS_DB_PORT=$CLOUDRON_MYSQL_PORT
export TTRSS_MYSQL_CHARSET=UTF8
export TTRSS_SELF_URL_PATH=$CLOUDRON_APP_ORIGIN
export TTRSS_SINGLE_USER_MODE=
export TTRSS_SIMPLE_UPDATE_MODE=
export TTRSS_AUTH_AUTO_CREATE=true
export TTRSS_LOG_DESTINATION=
export TTRSS_CHECK_FOR_UPDATES=
export TTRSS_SMTP_FROM_NAME="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Tiny Tiny RSS}"
export TTRSS_SMTP_FROM_ADDRESS=$CLOUDRON_MAIL_FROM
export TTRSS_DIGEST_SUBJECT="[tt-rss] New headlines for last 24 hours"
export TTRSS_SMTP_SERVER=${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}
export TTRSS_SMTP_LOGIN=$CLOUDRON_MAIL_SMTP_USERNAME
export TTRSS_SMTP_PASSWORD=$CLOUDRON_MAIL_SMTP_PASSWORD
export TTRSS_SMTP_SECURE=

# auth_internal is required for "password" login (app password for the mobile app to work)
# this has the side effect that a user can reset the password
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    export TTRSS_AUTH_OIDC_NAME=${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}
    export TTRSS_AUTH_OIDC_URL=${CLOUDRON_OIDC_ISSUER}
    export TTRSS_AUTH_OIDC_CLIENT_ID=${CLOUDRON_OIDC_CLIENT_ID}
    export TTRSS_AUTH_OIDC_CLIENT_SECRET=${CLOUDRON_OIDC_CLIENT_SECRET}
    export CLOUDRON_BUILTIN_PLUGINS="auth_internal, auth_oidc, mailer_smtp"
else
    export CLOUDRON_BUILTIN_PLUGINS="auth_internal, auth_oidc, mailer_smtp"
fi

source /app/data/env.sh
# export TTRSS variables for cron
declare -px | grep --color=never TTRSS_ > /run/ttrss/env.sh

echo "==> Creating config"
if [[ ! -f /app/data/config.php ]]; then
    cp /app/pkg/config.php.template /app/data/config.php
fi
# migration can be removed in next release
sed -e '/LDAP_/d' -i /app/data/config.php

echo "==> Changing permissions"
chown -R www-data:www-data /app/data/ /run/ttrss

echo "==> Updating schema"
echo "yes" | /usr/local/bin/gosu www-data:www-data php /app/code/update.php --update-schema

echo "==> Removing unused admin user"
mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "DELETE FROM ttrss_users WHERE login='admin'";

echo "==> Starting TTRSS"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
