# TinyTinyRSS Cloudron App

This repository contains the Cloudron app package source for [TinyTinyRSS](https://tt-rss.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.tt_rss.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.tt_rss.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd tinytinyrss-app

cloudron build
cloudron install
```

## Notes

* tt-rss now uses a rolling release model

* tt-rss themes require a fixed version header at the top of the theme's css.
  See includes/version.php in tt-rss code base (VERSION_STATIC).

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if things are ok.

```
cd tinytinyrss-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

